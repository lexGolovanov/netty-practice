package net.thumbtack.nettypractice.summing;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class SummingWebServerApplication {
    static final int SERVER_PORT = 8888;

    public static void main(String[] args) throws InterruptedException {
        final EventLoopGroup acceptanceGroup = new NioEventLoopGroup(1);
        final EventLoopGroup clientsGroup = new NioEventLoopGroup(3);

        try {
           final ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(acceptanceGroup, clientsGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new HttpServerInitializer())
                    .bind(SERVER_PORT)
                    .sync()
                    .channel()
                    .closeFuture().
                    sync();
        } finally {
            acceptanceGroup.shutdownGracefully();
            clientsGroup.shutdownGracefully();
        }
    }
}
