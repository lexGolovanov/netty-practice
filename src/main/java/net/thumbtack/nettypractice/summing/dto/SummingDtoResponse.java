package net.thumbtack.nettypractice.summing.dto;

public class SummingDtoResponse {
    private int sum;

    public SummingDtoResponse(int sum) {
        this.sum = sum;
    }

    public int getSum() {
        return sum;
    }
}
