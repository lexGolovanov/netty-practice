package net.thumbtack.nettypractice.summing.dto;

public class SummingDtoRequest {
    private int x;
    private int y;

    public SummingDtoRequest(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
