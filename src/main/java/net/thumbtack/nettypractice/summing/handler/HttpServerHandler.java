package net.thumbtack.nettypractice.summing.handler;

import net.thumbtack.nettypractice.summing.dto.SummingDtoRequest;

import com.google.gson.Gson;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.util.CharsetUtil;

import static io.netty.handler.codec.http.HttpHeaderNames.*;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

public class HttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private final Gson GSON = new Gson();

    private static final String CONTENT_TYPE_VALUE = "text/html; charset=UTF-8";
    private static final String CONNECTION_VALUE = "close";
    private static final String RESPONSE_CONTENT = "<html><body><h1>Sum: %d</h1></body></html>";

    @Override
    public void channelReadComplete(ChannelHandlerContext clientContext) {
        clientContext.flush();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext clientContext, FullHttpRequest httpRequest) {
        final String requestJson = httpRequest.content().toString(CharsetUtil.UTF_8);
        final SummingDtoRequest request = GSON.fromJson(requestJson, SummingDtoRequest.class);
        int sum = request.getX() + request.getY();
        final String response = String.format(RESPONSE_CONTENT, sum);

        final FullHttpResponse httpResponse = new DefaultFullHttpResponse(
                HTTP_1_1,
                OK,
                Unpooled.copiedBuffer(response, CharsetUtil.UTF_8)
        );
        httpResponse.headers().set(CONTENT_TYPE, CONTENT_TYPE_VALUE);
        httpResponse.headers().set(CONTENT_LENGTH, httpResponse.content().readableBytes());
        httpResponse.headers().set(CONNECTION, CONNECTION_VALUE);

        clientContext.write(httpResponse);
        clientContext.writeAndFlush(Unpooled.EMPTY_BUFFER)
                .addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext clientContext, Throwable cause) {
        cause.printStackTrace();
        clientContext.close();
    }
}
